# README

This is my guest lecture for an applied cryptography course, 2024.  The topic is
Certificate Transparency and Merkle Trees.  Sigsum is a bonus towards the end.

The used template is copied from the Sigsum project's [documentation repo][].

[documentation repo]: https://gitlab.sigsum.org/sigsum/project/documentation/-/tree/main/beamer

## Compiled slides in handout format

See [handout](./handout.pdf).

## Few pointers to self

  - Passport analogy to introduce certificates and CAs
  - Explain DV issuance on whiteboard when starting DigiNotar story
  - Highlight the impact of the DigiNotar hack for Iranian users
  - Append-only list
    - Show it on the whiteboard and what is not allowed.
    - Emphasize: pretty nice to have a concise list of all certificates!  Refer
      back to the gmail.com and jabber.ru interceptions introduced earlier.
  - Segway: so, how would one get a certificate into the list?
  - ...UAs, monitors, tab out to show crt.sh, auditors, Merkle trees.
  - Show how Merkle tree grows on whiteboard; size 1,2,3,4,8
  - Inclusion proof on tree of size 8, then O(log) complexity is more obvious
  - ...How CT might have worked in an alternative world, challenges
  - Real-world deployment: sink slide, can be long or short.  Part of the story
    here and the three slides before is: ``applied'' and ``systems'' stuff is
    harder than the theory of a Merkle tree where challenges are hand-waved.
  - Despite challenges, tell the story of how CT as is = still a success, and it
    gradually gets better.
  - Wrap up -- now they should be able to understand the CT site.  Go through
    the picture as summary.  Hopefully they will remember the problem, the
    solution idea, and that if they have certificates please also monitor them!
